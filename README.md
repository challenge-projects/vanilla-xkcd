# React - XKCD

Esta es la solución para un ejercicio planteado en el cual consistia en consumir la API de XKCD

### Pre-requisitos 📋

Unicamente es necesario bases fundamentales en hojas de estilos css con Sass, tambien el uso de la metodología BEM y manejo de estructuras HTML, los fundamentos de JavaScript vanilla.

_Ejemplo BEM :_
```
<p class="home__description--mobile">Un podcast que explora el mundo de la programación. Nuevos episodios, todos los jueves cada 15 días.</p>
```

## Construido con 🛠️

Estas fueron las herramientas usadas para el desarrollo de este proyecto:

* [Sass](https://sass-lang.com/) - El framework css usado
* [BEM](http://getbem.com/) - Metodología Css
* [Fontawesome](https://fontawesome.com/) - Fuente para icons
* [Google Fonts](https://fonts.google.com/) - Fuentes
* [Axion](https://reactjs.org/) - Libreria para consumo de API


## Uso 🎮

Podra calificar el comic de 1 - 5 estrellas, 
una vez este sea calificado, podrá visualizar un botón,
el cual le permitira cargar un nuevo comic.
## Licencia 📄

Este proyecto está bajo la Licencia  MIT .


---
⌨️ por [David Moreno](https://www.linkedin.com/in/davidmorenocode/) 🤓☕
