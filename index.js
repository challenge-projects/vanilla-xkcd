const comicImg = document.getElementById('comicImg');
const comicTitle = document.getElementById('comicTitle');
const comicPhrase = document.getElementById('comicPhrase');
const comicButton = document.getElementById('comicButton');
const comicRating = document.querySelector('.comic__rating');
const getAllStar = comicRating.querySelectorAll('.comic__star');
const arrPhrases = ['Oops!', 'Si, puede mejorar!', 'No está mal!', 'Grandioso!', 'Excelente!'];

const numRandom = () => Math.floor(Math.random() * Math.floor(100));

const initialize = () =>{
  clean();
  const comicId = numRandom();  
  getComic(comicId);
}

const getComic = async (comicId) =>{
  const API = `https://cors-anywhere.herokuapp.com/https://xkcd.com/${comicId}/info.0.json`;
  const resPost = await axios(API);
  setComic(resPost.data);
}

const setComic = ({img, title}) =>{
  comicImg.setAttribute('src', img);
  comicTitle.innerText = title;
}

const score = (event) =>{
  if(event.target.classList.contains('fas')){

    const countStar = parseInt(event.target.dataset.starValue);

    getAllStar.forEach((star, key) =>{
      if(( key + 1 ) <= countStar){
        star.classList.add('comic__star--checked');
      }else{
        star.classList.remove('comic__star--checked');
      }
    })
    comicPhrase.innerText = arrPhrases[(countStar - 1)];
    comicPhrase.classList.remove('comic__title--inactive');
    comicButton.classList.remove('comic__button--inactive');
  }
};

const clean = () =>{
  comicPhrase.classList.add('comic__title--inactive');
  comicButton.classList.add('comic__button--inactive');

  getAllStar.forEach((star) =>{
    star.classList.remove('comic__star--checked');
  });
}

comicButton.addEventListener('click', initialize);
comicRating.addEventListener('click', score);

initialize();